package com.example.stefano.sleeptimes;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class main extends AppCompatActivity {

    Button btn;
    EditText et;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        btn = (Button) findViewById(R.id.button);
        et = (EditText) findViewById(R.id.editText);
        tv = (TextView) findViewById(R.id.tv2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadingStuffTask task = new LoadingStuffTask();
                task.execute((String) et.getText().toString());
            }
        });
    }
    
    private class LoadingStuffTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute(){
            tv.setText(getString(R.string.slp));
        }

        @Override
        protected String doInBackground(String... params) {
            SystemClock.sleep(Integer.parseInt(params[0]));
            return "Finito";
        }

        @Override
        protected void onPostExecute(String result) {
            String text = (String) et.getText().toString();
            tv.setText(getString(R.string.slt) + text + getString(R.string.msec));
        }
    }

}
